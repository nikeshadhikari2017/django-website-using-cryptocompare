from django.shortcuts import render

# Create your views here.

def home(request):
    import requests
    import json

     #api for the news cryptocompare
    price_request = requests.get("https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC,XRP&tsyms=USD")
    price = json.loads(price_request.content)

    #api for the news cryptocompare
    api_request = requests.get("https://min-api.cryptocompare.com/data/v2/news/?lang=EN")
    api = json.loads(api_request.content)
    return render (request, 'home.html', {'api':api, 'price':price})


def prices(request):
    if request.method =='POST':
        quote = request.POST['quote']
        quote = quote.upper()

        import requests
        import json

        #api for the news cryptocompare
        crypto_request = requests.get("https://min-api.cryptocompare.com/data/pricemultifull?fsyms="+ quote +"&tsyms=USD")
        crypto = json.loads(crypto_request.content)
        return render (request, 'prices.html', {'quote': quote, 'crypto':crypto})
    else:
        notfound = "Please search the crypto for the information."
        return render(request, 'prices.html', {'notfound': notfound})